#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/aosp_fleur.mk

COMMON_LUNCH_CHOICES := \
    aosp_fleur-user \
    aosp_fleur-userdebug \
    aosp_fleur-eng
